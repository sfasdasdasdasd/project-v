﻿package _S_K_ {

    import _sp._L_A_;

    import flash.events.Event;
    import flash.events.IEventDispatcher;

    public interface _j3 extends _L_A_ {

    function get _S_b():String;

    function get eventClass():Class;

    function get target():IEventDispatcher;

    function set target(_arg1:IEventDispatcher):void;

    function dispatchEvent(_arg1:Event):Boolean;

}
}