﻿package _zD_ {

    import _05Z_._08i;

    import _0L_C_._qO_;

    import _0_j._kW_;

    import _C__._cM_;

    import _D_d._hj;

    import _U_5._dd;

    import _W_D_._0I_H_;

    import _ke.Buttons;

    import ui.views.CreditsView;
    import ui.views.CurrentCharacterScreen;
    import ui.views.LegendsView;
    import ui.views.ServersView;
    import ui.views.TitleView;

    public class __for extends _cM_ {

    [Inject]
    public var view:TitleView;
    [Inject]
    public var _eJ_:_0I_H_;
    [Inject]
    public var _T__:_dd;
    [Inject]
    public var _D_u:_08i;

    override public function initialize():void {
        this.view._ft.add(this._F_A_);
        this.view.initialize(this._eJ_._T_1);
    }

    override public function destroy():void {
        this.view._ft.remove(this._F_A_);
    }

    public function _F_A_(_arg1:String):void {
        switch (_arg1) {
            case Buttons.PLAY:
                this._04P_();
                return;
            case Buttons.SERVERS:
                this._0B_M_();
                return;
            case Buttons.CREDITS:
                this._C_0();
                return;
            case Buttons.ACCOUNT:
                this._0A_3();
                return;
            case Buttons.LEGENDS:
                this._N_E_();
                return;
            case Buttons.EDITOR:
                this._0E_r();
                return;
            case Buttons.SPRITE:
                this._1E_r();
                return;
        }
    }

    private function _04P_():void {
        var _local1:_qO_;
        if (this._eJ_._T_1.servers_.length == 0) {

                _local1 = new _qO_((((("The Project V is currently offline.\n\n" + "Go here for more information:\n") + '<font color="#7777EE">') + '<a href="http://forum.kithio.com/">') + "forum.kithio.com</a></font>."), "Offline", null, null, "/offLine");
            this.view.stage.addChild(_local1);
            return;
        }

        this._T__.dispatch(new CurrentCharacterScreen());
    }

    private function _C_0():void {
        this._T__.dispatch(new CreditsView());
    }

    private function _0B_M_():void {
        this._T__.dispatch(new ServersView());
    }

    private function _0A_3():void {
        this.view._0j();
    }

    private function _N_E_():void {
        this._T__.dispatch(new LegendsView());
    }

    private function _0E_r():void {
        this._T__.dispatch(new _hj());
    }

    private function _1E_r():void {
        this._T__.dispatch(new _kW_());
    }

}
}