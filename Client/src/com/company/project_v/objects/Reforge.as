package com.company.project_v.objects {

    import com.company.project_v.game.GameSprite;

    import ui.panels.Panel;
    import ui.panels.ReforgePanel;

    public class Reforge extends GameObject implements _G_4 {
    public function Reforge(_arg1:XML) {
        super(_arg1);
        this._064 = true;
    }

    public function getPanel(_arg1:GameSprite):Panel {
        return new ReforgePanel(_arg1, this);
    }
}
}