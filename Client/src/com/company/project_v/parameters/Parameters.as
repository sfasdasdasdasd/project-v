﻿




package com.company.project_v.parameters {

    import com.company.utils._0A_s;
    import com.company.utils.Keys;

    import flash.display.DisplayObject;
    import flash.net.LocalConnection;
    import flash.net.SharedObject;
    import flash.utils.Dictionary;

    public class Parameters {

    public static const standalone:Boolean = false;

    public static const isTesting:Boolean = true;
    public static const sendErrors:Boolean = false;
    public static const clientVersion:String = "1.2";
    public static const _wZ_:Boolean = true;
    public static const gamePort:int = 2050;
    public static const _1h:Boolean = false;
    public static const _1C_c:uint = 0xE678CC;
    public static const _mg:uint = 10944349;
    public static const _8T_:uint = 0xFCDF00;
    public static const partyColor:uint = 0x709BFF;
    public static const _F_g:int = 60;
    public static const RotationSpeed:Number = 0.003;
    public static const _E_S_:int = 20;
    public static const SendInfo:String = "";
    public static const SendClient:String = "*Client*";
    public static const SendError:String = "*Error*";
    public static const SendHelp:String = "*Help*";
    public static const SendGuild:String = "*Guild*";
    public static const SendParty:String = "*Party*";
    public static const _0u:int = 1000;
    public static const _0H_m:int = 1000;
    public static const NEXUS_ID:int = -2;
    public static const TEST_ID:int = -6;
    public static const _K_5:Number = 18;
    public static const ToS_Url_:String = "http://www.realmofthemadgod.com/TermsofUse.html";
    public static const musicUrl_:String = "travoos.com";
    public static const connection:String = _fK_();
    public static const HelpCommand:String = "Help:\n" + "[/pause]: pause the game (until you [/pause] again\n" + "[/who]: list players online\n" + "[/tutorial]: enter the tutorial\n" + "[/yell <message>]: send message to all players in Nexus\n" + "[/tell <player name> <message>]: send a private message to a player\n" + "[/guild <message>]: send a message to your guild\n" + "[/ignore <player name>]: don't show chat messages from player\n" + "[/unignore <player name>]: stop ignoring a player\n" + "[/teleport <player name>]: teleport to a player\n" + "[/trade <player name>]: request a trade with a player\n" + "[/invite <player name>]: invite a player to your guild\n" + "[/join <guild name>]: join a guild (invite necessary\n" + "[/?]: this message";
    public static const RANDOM1:String = "311f80691451c71b09a13a2a6e";
    public static const RANDOM2:String = "72c5583cafb6818995cbd74b80";
    public static const RSAKey:String = "-----BEGIN PUBLIC KEY-----\n" + "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzRQGnNw5kEaIpych+U4J" + "ZtuLnMJuQKEyU1FfcyHISNkYhy1jmSMPav+7D3Qg1R6exeeM6LtXvUEQ6Fgris1J" + "UVDr5gjII0ti98LvIsF/hgTk2VOvm2QK/1OpyAxni4RZuR8OE0p0Co0yUwhTVjfN" + "5Ooh4hntfuwVtk1AZEFudA9oOqdUbNwi98M9VIf+9vJWnx2zAx+1r7Ja/JDeM3Fb" + "YW9awMw4d0fpGoN/PfZGqKdgd4jjXLuhatSu13sAU39aPZAI2ku3MQRMQcUBLxDI" + "HyEq1NA+BztASjpYr3w+bX85bWZwPOBN95O3z6hbDTv/FZj5tpkVqaVhxqBeEbaR" + "ywIDAQAB\n" + "-----END PUBLIC KEY-----";
    public static const x101010:uint = 0x101010;
    public static const x161616:uint = 0x161616;
    public static const x202020:uint = 0x202020;
    public static const x242424:uint = 0x242424;
    //public static const AccentColor:uint = null;

    public static var root:DisplayObject;
    public static var data_:Object = null;
    public static var _R_P_:int = 1;
    public static var _Q_b:int = 0;
    public static var _Q_w:Boolean = true;
    public static var _0F_o:Boolean = false;
    public static var _ih:Boolean = false;
    public static var _hk:Boolean = true;
    private static var _Z_U_:SharedObject = null;
    private static var _C_o:Dictionary = new Dictionary();

    public static function _02Q_():String {
        return (Parameters.isTesting ? "Production" : "Devel") + " Build #" + Parameters.clientVersion + (_I_O_() ? " Testing" : "");
    }

    public static function _I_O_():Boolean {
        var _local3:LocalConnection;
        var _local1:Boolean;
        _local3 = new LocalConnection();
        _local1 = !(_local3.domain == "realm.kithio.com");
        _local1 = !(standalone);
        return (_local1);
    }

    public static function _fK_():String {
        return ("127.0.0.1:8080");
    }


    public static function load():void {
        try {
            _Z_U_ = SharedObject.getLocal("ProjectV", "/");
            data_ = _Z_U_.data;
        } catch (error:Error) {
            data_ = {};
        }
        _fX_();
        save();
    }

    public static function save():void {
        try {
            if (_Z_U_ != null) {
                _Z_U_.flush();
            }
        } catch (error:Error) {
        }
    }

    public static function setKey(_arg1:String, _arg2:uint):void {
        var _local3:String;
        for (_local3 in _C_o) {
            if (data_[_local3] == _arg2) {
                data_[_local3] = Keys._0F_r;
            }
        }
        data_[_arg1] = _arg2;
    }

    public static function _fX_():void {
        _la("moveLeft", Keys.A);
        _la("moveRight", Keys.D);
        _la("moveUp", Keys.W);
        _la("moveDown", Keys.S);
        _la("rotateLeft", Keys.Q);
        _la("rotateRight", Keys.E);
        _la("useSpecial", Keys.SPACE);
        _la("interact", Keys.NUMBER_0);
        _la("useInvSlot1", Keys.NUMBER_1);
        _la("useInvSlot2", Keys.NUMBER_2);
        _la("useInvSlot3", Keys.NUMBER_3);
        _la("useInvSlot4", Keys.NUMBER_4);
        _la("useInvSlot5", Keys.NUMBER_5);
        _la("useInvSlot6", Keys.NUMBER_6);
        _la("useInvSlot7", Keys.NUMBER_7);
        _la("useInvSlot8", Keys.NUMBER_8);
        _la("escapeToNexus", Keys.INSERT);
        _la("autofireToggle", Keys.I);
        _la("scrollChatUp", Keys._R_0);
        _la("scrollChatDown", Keys._xs);
        _la("miniMapZoomOut", Keys._0F_K_);
        _la("miniMapZoomIn", Keys._0E_f);
        _la("resetToDefaultCameraAngle", Keys.R);
        _la("togglePerformanceStats", Keys._0F_r);
        _la("options", Keys.O);
        _la("toggleCentering", Keys._0F_r);
        _la("chat", Keys.ENTER);
        _la("chatCommand", Keys._jE_);
        _la("tell", Keys.TAB);
        _la("guildChat", Keys.G);
        _la("partyChat", Keys.P);
        _la("toggleFullscreen", Keys._0F_r);
        _la("switchTabs", Keys.B);
        _la("giveItem", Keys._0F_r);
        _iw("playerObjectType", 782);
        _iw("playMusic", true);
        _iw("playSFX", true);
        _iw("playPewPew", true);
        _iw("centerOnPlayer", true);
        _iw("preferredServer", null);
        _iw("needsTutorial", false);
        _iw("needsNexusTutorial", false);
        _iw("needsRandomRealm", false);
        _iw("cameraAngle", ((7 * Math.PI) / 4));
        _iw("defaultCameraAngle", ((7 * Math.PI) / 4));
        _iw("showQuestPortraits", true);
        _iw("fullscreenMode", false);
        _iw("showProtips", false);
        _iw("protipIndex", 0);
        _iw("joinDate", _0A_s._mP_());
        _iw("lastDailyAnalytics", null);
        _iw("allowRotation", false);
        _iw("charIdUseMap", {});
        _iw("drawShadows", false);
        _iw("textBubbles", true);
        _iw("showTradePopup", true);
        _iw("paymentMethod", null);
        _iw("filterLanguage", true);
        _iw("showGuildInvitePopup", true);
        _iw("showBeginnersOffer", false);
        _iw("beginnersOfferTimeLeft", 0);
        _iw("beginnersOfferShowNow", false);
        _iw("beginnersOfferShowNowTime", 0);
        _iw("watchForTutorialExit", false);
        _iw("contextualClick", true);
        _iw("inventorySwap", true);
        _iw("hidePlayerChat", false);
        _iw("chatStarRequirement", 1);
        _iw("togglePercentage", false);
        _iw("toggleBarText", true);
        _iw("clickForGold", false);
        _iw("rotationSpeed", 0.003);

    }

    private static function _la(_arg1:String, _arg2:uint):void {
        if (!data_.hasOwnProperty(_arg1)) {
            data_[_arg1] = _arg2;
        }
        _C_o[_arg1] = true;
    }

    private static function _iw(_arg1:String, _arg2:*):void {
        if (!data_.hasOwnProperty(_arg1)) {
            data_[_arg1] = _arg2;
        }
    }

}
}