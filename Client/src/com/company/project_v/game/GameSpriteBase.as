package com.company.project_v.game {

    import com.company.project_v.map.MapHandler;
    import com.company.project_v.map.View;
    import com.company.project_v.objects.GameObject;

    import flash.display.Sprite;

    import networking._1f;
    import networking.packets.server.MapInfo;

    public class GameSpriteBase extends Sprite {

    public var isEditor:Boolean;
    public var mui_:_07a;
    public var lastUpdate_:int = 0;
    public var moveRecords_:_uw;
    public var map:MapHandler;
    public var camera_:View;
    public var gsc_:_1f;

    public function GameSpriteBase() {
        this.moveRecords_ = new _uw();
        this.camera_ = new View();
        super();
    }
    public function initialize():void{
    }
    public function setFocus(_arg1:GameObject):void{
    }
    public function applyMapInfo(_arg1:MapInfo):void{
    }
    public function evalIsNotInCombatMapArea():Boolean{
        return (false);
    }
}
}
