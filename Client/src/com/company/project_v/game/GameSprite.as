﻿




package com.company.project_v.game {

    import _U_5._D_L_;
    import _U_5._zz;

    import _qN_.Account;

    import _vf.Music;

    import com.company.project_v.appengine._0K_R_;
    import com.company.project_v.map.Map;
    import com.company.project_v.map.View;
    import com.company.project_v.objects.Player;
    import com.company.project_v.objects.Projectile;
    import com.company.project_v.parameters.Parameters;
    import com.company.project_v.util.TextureRedrawer;
    import com.company.utils.MoreColorUtil;
    import com.company.utils._G_;
    import com.company.utils._H_U_;

    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.filters.ColorMatrixFilter;
    import flash.utils.ByteArray;
    import flash.utils.getTimer;

    import networking.Server;
    import networking._1f;
    import networking.packets.server.MapInfo;

    import ui.elements.Chat;
    import ui.elements.CurrencyDisplay;
    import ui.elements.GuildDisplay;
    import ui.elements.HudView;
    import ui.elements.RankDisplay;
    import ui.views.MapLoadingScreen;

    public class GameSprite extends GameSpriteBase {

    public static const _0F_s:Number = new Date().time;
    protected static const _oj:ColorMatrixFilter = new ColorMatrixFilter(MoreColorUtil._0M_l);

    public function GameSprite(_arg1:Server, _arg2:int, _arg3:Boolean, _arg4:int, _arg5:int, _arg6:ByteArray, _arg7:_0K_R_, _arg8:String) {
        this.camera_ = new View();
        this.moveRecords_ = new _uw();
        super();
        this.charList_ = _arg7;
        this.map_ = new Map(this);
        addChild(this.map_);
        this.gsc_ = new _1f(this, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg8);
        this.mui_ = new _07a(this);
        this._chat = new Chat(this, 600, 600);
        addChild(this._chat);
        this.hudView = new HudView(this, 200, 600);
        addChild(this.hudView);
        this._IdleWatcher = new IdleWatcher();
        addEventListener(Event.ADDED_TO_STAGE, this.connect);
        addEventListener(Event.REMOVED_FROM_STAGE, this.disconnect);
    }
    public var _chat:Chat;
    public var hudView:HudView;
    public var charList_:_0K_R_;
    public var isNexus_:Boolean = false;
    public var _IdleWatcher:IdleWatcher;
    public var rankDisplay:RankDisplay;
    public var guildDisplay:GuildDisplay;
    public var currencyDisplay:CurrencyDisplay;
    private var _map:Map;
    private var _bA_:int = 0;
    private var _qA_:int = 0;
    private var mapLoadingScreen:MapLoadingScreen;
    private var _connected:Boolean;

    public function get map_():Map {
        return (this._map);
    }

    public function set map_(_arg1:Map):void {
        this._map = _arg1;
    }

    public function _S_z(_arg1:MapInfo):void {
        this._map.setProps(_arg1.width_, _arg1.height_, _arg1.name_, _arg1.background_, _arg1.allowPlayerTeleport_, _arg1.showDisplays_, _arg1.music_);
        this._dO_(_arg1);
    }

    public function _dO_(_arg1:MapInfo):void {
        if (!this.mapLoadingScreen) {
            this.mapLoadingScreen = new MapLoadingScreen();
        }
        addChild(this.mapLoadingScreen);
        _D_L_.getInstance().dispatch(_arg1);
    }

    override public function initialize():void {
        this.map_.initialize();
        this.hudView.initialize();

        if (this.map_.showDisplays_) {
            this.currencyDisplay = new CurrencyDisplay(this);
            this.currencyDisplay.x = 594;
            this.currencyDisplay.y = 0;
            addChild(this.currencyDisplay);
            this.rankDisplay = new RankDisplay(-1, true, false);
            this.rankDisplay.x = 8;
            this.rankDisplay.y = 4;
            addChild(this.rankDisplay);
            this.guildDisplay = new GuildDisplay("", -1);
            this.guildDisplay.x = 64;
            this.guildDisplay.y = 6;
            addChild(this.guildDisplay);
        }
        this.isNexus_ = (this.map_.name_ == "Nexus" || this.map_.name_ == "Shop" || this.map_.name_ == "Editor");

        var _local2:Account = Account._get();
        var _local3:Object = {
            "game_net_user_id": _local2.gameNetworkUserId(),
            "game_net": _local2.gameNetwork(),
            "play_platform": _local2.playPlatform()
        };
        _H_U_._t2(_local3, Account._get().credentials());

        this._02J_();
    }


    public function dispose():void {
        ((contains(this.map_)) && (removeChild(this.map_)));
        this.map_.dispose();
        removeChild(this.hudView);
        this.hudView.dispose();
        _G_.clear();
        TextureRedrawer.clearCache();
        Projectile.dispose();
    }


    private function _02J_():void {
        if (this.mapLoadingScreen) {
            this.mapLoadingScreen._pW_();
            this.mapLoadingScreen = null;
        }
    }

    public function connect(_arg1:Event):void {
        if (!this._connected) {
            this._connected = true;
            this._connected = true;
            this.hudView.x = 600;
            this.hudView.y = 0;
            this.gsc_.connect();
            this._IdleWatcher.start(this);
            this.lastUpdate_ = getTimer();
            stage.addEventListener(Event.ENTER_FRAME, this.onEnterFrame);
            if (Parameters.data_["mscale"] == undefined) {
                Parameters.data_["mscale"] = "1.0";
                Parameters.save();
            }
            if (Parameters.data_["stageScale"] == undefined) {
                Parameters.data_["stageScale"] = StageScaleMode.NO_SCALE;
                Parameters.save();
            }
            if (Parameters.data_["uiscale"] == undefined) {
                Parameters.data_["uiscale"] = true;
                Parameters.save();
            }
            stage.scaleMode = Parameters.data_["stageScale"];
            stage.addEventListener(Event.RESIZE, this.onScreenResize);
            stage.dispatchEvent(new Event(Event.RESIZE));
        }
    }

    public function disconnect(_arg1:Event):void {
        if (this._connected) {
            this._connected = false;
            this._IdleWatcher.stop();
            this.gsc_._08._0F_G_();
            _zz.instance.dispatch();
            stage.removeEventListener(Event.ENTER_FRAME, this.onEnterFrame);
            dispose();
        }
    }

    public function onScreenResize(_arg1:Event):void {
        var _local2:Boolean = Parameters.data_["uiscale"];
        var _local3:Boolean = true;
        var _local4:Boolean = true;
        var _local5:Boolean = true;
        var _local6:Boolean = true;
        var _local7:Boolean = true;
        var _local8:Boolean = true;
        var _local12:Number = (800 / stage.stageWidth);
        var _local13:Number = (600 / stage.stageHeight);
        if (((!((this._map == null))) && (_local3))) {
            this._map.scaleX = (_local12 * (((stage.scaleMode) != StageScaleMode.EXACT_FIT) ? Parameters.data_["mscale"] : 1));
            this._map.scaleY = (_local13 * (((stage.scaleMode) != StageScaleMode.EXACT_FIT) ? Parameters.data_["mscale"] : 1));
        }
        if (this.hudView != null) {
            if (_local2) {
                this.hudView.scaleX = (_local12 / _local13);
                this.hudView.scaleY = 1;
                this.hudView.y = 0;
            } else {
                if (_local4) {
                    this.hudView.scaleX = _local12;
                    this.hudView.scaleY = _local13;
                    this.hudView.y = (300 * (1 - _local13));
                }
            }
            this.hudView.x = (800 - (200 * this.hudView.scaleX));
            if (this.currencyDisplay != null) {
                this.currencyDisplay.x = (this.hudView.x - (6 * this.currencyDisplay.scaleX));
            }
        }
        if (this._chat != null) {
            if (_local2) {
                this._chat.scaleX = (_local12 / _local13);
                this._chat.scaleY = 1;
            } else {
                if (_local5) {
                    this._chat.scaleX = _local12;
                    this._chat.scaleY = _local13;
                }
            }
        }

        if (this.rankDisplay != null) {
            if (_local2) {
                this.rankDisplay.scaleX = (_local12 / _local13);
                this.rankDisplay.scaleY = 1;
            } else {
                if (_local6) {
                    this.rankDisplay.scaleX = _local12;
                    this.rankDisplay.scaleY = _local13;
                }
            }
            this.rankDisplay.x = (8 * this.rankDisplay.scaleX);
            this.rankDisplay.y = (4 * this.rankDisplay.scaleY);
        }
        if (this.guildDisplay != null) {
            if (_local2) {
                this.guildDisplay.scaleX = (_local12 / _local13);
                this.guildDisplay.scaleY = 1;
            } else {
                if (_local7) {
                    this.guildDisplay.scaleX = _local12;
                    this.guildDisplay.scaleY = _local13;
                }
            }
            this.guildDisplay.x = (this.rankDisplay.width + (16 * this.guildDisplay.scaleX));
            this.guildDisplay.y = (6 * this.guildDisplay.scaleY);
        }

        if (this.currencyDisplay != null) {
            if (_local2) {
                this.currencyDisplay.scaleX = (_local12 / _local13);
                this.currencyDisplay.scaleY = 1;
            } else {
                if (_local8) {
                    this.currencyDisplay.scaleX = _local12;
                    this.currencyDisplay.scaleY = _local13;
                }
            }
        }
    }

    override public function evalIsNotInCombatMapArea():Boolean{
        return ((_map.name_ == "Nexus") || (_map.name_ == "Vault") || (_map.name_ == "Guild Hall") || (_map.name_ == "The Shop") || (_map.name_ == "Nexus Tutorial"));
    }

    private function onEnterFrame(_arg1:Event):void {
        var _local2:int = getTimer();
        var _local3:int = (_local2 - this.lastUpdate_);
        if (this._IdleWatcher.update(_local3)) {
            dispatchEvent(new Event(Event.COMPLETE));
            return;
        }
        this._bA_ = (this._bA_ + _local3);
        this._qA_ = (this._qA_ + 1);
        if (this._bA_ > 300000) {
            this._qA_ = 0;
            this._bA_ = 0;
        }
        Music.updateFade();
        this.map_.update(_local2, _local3);
        this.camera_.update(_local3);
        onScreenResize(_arg1);
        var _local4:Player = this.map_.player_;
        if (_local4 != null) {
            this.camera_.InitView(_local4);
            this.map_.draw(this.camera_, _local2);
            this.hudView.draw();
            if (this.map_.showDisplays_) {
                this.rankDisplay.draw(_local4.numStars_);
                this.guildDisplay.draw(_local4.guildName_, _local4.guildRank_);
                this.currencyDisplay.draw(_local4.credits_, _local4.fame_);
            }
            if (_local4.isPaused()) {
                this.map_.filters = [_oj];
                this.hudView.filters = [_oj];
                this.map_.mouseEnabled = false;
                this.map_.mouseChildren = false;
                this.hudView.mouseEnabled = false;
                this.hudView.mouseChildren = false;
            } else {
                if (this.map_.filters.length > 0) {
                    this.map_.filters = [];
                    this.hudView.filters = [];
                    this.map_.mouseEnabled = true;
                    this.map_.mouseChildren = true;
                    this.hudView.mouseEnabled = true;
                    this.hudView.mouseChildren = true;
                }
            }
            this.moveRecords_._F_5(_local2, _local4.x_, _local4.y_);
        }
        this.lastUpdate_ = _local2;
    }

}
}