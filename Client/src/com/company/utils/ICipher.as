﻿// Decompiled by AS3 Sorcerer 1.99
// http://www.as3sorcerer.com/

//com.company.utils.ICipher

package com.company.utils {

    import flash.utils.ByteArray;

    public interface ICipher {

    function getBlockSize():uint;

    function encrypt(_arg1:ByteArray):void;

    function decrypt(_arg1:ByteArray):void;

    function dispose():void;

    function toString():String;

}
}//package com.hurlant.crypto.symmetric

