﻿// Decompiled by AS3 Sorcerer 1.99
// http://www.as3sorcerer.com/

//com.company.utils.IAsn1Type

package com.company.utils {

    import flash.utils.ByteArray;

    public interface IAsn1Type {

    function getType():uint;

    function getLength():uint;

    function toDER():ByteArray;

}
}//package com.hurlant.utils.der

