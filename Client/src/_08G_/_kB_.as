﻿




package _08G_ {

    import _03T_._078;

    import _0_p.IContext;
    import _0_p._D_v;
    import _0_p._Q_5;

    import _eZ_.IInjector;

    import _g3.instanceOf;

    import flash.display.DisplayObjectContainer;

    public class _kB_ implements _D_v {

    private const _ul:String = _078.create(_kB_);

    private var _vz:IInjector;
    private var _eo:_Q_5;

    public function extend(_arg1:IContext):void {
        this._vz = _arg1.injector;
        this._eo = _arg1._0H_4(this);
        _arg1._ty(instanceOf(DisplayObjectContainer), this._uI_);
    }

    public function toString():String {
        return (this._ul);
    }

    private function _uI_(_arg1:DisplayObjectContainer):void {
        if (this._vz._d6(DisplayObjectContainer)) {
            this._eo._0E_Q_("A contextView has already been mapped, ignoring {0}", [_arg1]);
        } else {
            this._eo.debug("Mapping {0} as contextView", [_arg1]);
            this._vz.map(DisplayObjectContainer)._q3(_arg1);
        }
    }

}
}//package _08G_

