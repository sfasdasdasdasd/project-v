﻿package {

    import _05G_._X_G_;

    import _0_p.IContext;

    import _9u._074;

    import _C_5._tt;

    import _C__._07U_;

    import _G_A_.GameContext;
    import _G_A_._F_y;

    import _I_j._V_4;

    import _R_Q_._0K_S_;

    import _T_o._083;

    import _U_._K_a;

    import _U_5._D_c;

    import com.company.project_v.parameters.Parameters;
    import com.company.project_v.util.Dispatcher;
    import com.company.project_v.util.loadEmbeds;

    import flash.display.LoaderInfo;
    import flash.display.Sprite;
    import flash.display.Stage;
    import flash.display.StageScaleMode;
    import flash.events.*;

    [SWF(width="800", height="600", frameRate="60", backgroundColor="#000000")]
    public class Main extends Sprite {

        public static var primStage:Stage;
        public static var instance:Main;
        public static var sWidth:Number = 800;
        public static var sHeight:Number = 600;

        protected var context:IContext;

        public function Main() {
            instance = this;
            this.randomizeLoaderHeader();
            if (stage) {
                stage.addEventListener(Event.RESIZE, this.onStageResize);
                this.setup();
            } else {
                addEventListener(Event.ADDED_TO_STAGE, this.onAddedToStage);
            }
        }

        public function randomizeLoaderHeader():void {
            var _local1:int;
            while (_local1 < 21) {
                this.loaderInfo.bytes[_local1] = uint((Math.random() * 0xFF));
                _local1++;
            }
            this.loaderInfo.bytes[2] = 234;
        }

        public function onStageResize(_arg1:Event):void {
            if (stage.scaleMode == StageScaleMode.NO_SCALE)
            {
                scaleX = (stage.stageWidth / 800);
                scaleY = (stage.stageHeight / 600);
                x = ((800 - stage.stageWidth) / 2);
                y = ((600 - stage.stageHeight) / 2);
            }
            else
            {
                scaleX = 1;
                scaleY = 1;
                x = 0;
                y = 0;
            }
            sWidth = stage.stageWidth;
            sHeight = stage.stageHeight;
        }

        private function onAddedToStage(_arg1:Event):void {
            removeEventListener(Event.ADDED_TO_STAGE, this.onAddedToStage);
            this.setup();
        }
        private function setup():void {
            this.SetRoot();
            this.Configure();
            new loadEmbeds().load();
            stage.scaleMode = StageScaleMode.NO_SCALE;
            this.context.injector.getInstance(_D_c).dispatch();
            primStage = stage;
            scaleX = this.scaleX;
            scaleY = this.scaleY;
        }

        private function SetRoot():void {
            Parameters.root = stage.root;
        }

        private function Configure():void {
            this.context = new GameContext();
            this.context.injector.map(LoaderInfo)._q3(root.stage.root.loaderInfo);
            var _local1:Dispatcher = new Dispatcher(this);
            this.context.injector.map(Dispatcher)._q3(_local1);
            this.context.extend(_07U_).extend(_083)
                    .configure(_F_y).configure(_074)
                    .configure(_tt).configure(_K_a)
                    .configure(_0K_S_).configure(_V_4)
                    .configure(_X_G_)
                    .configure(this);
        }

    }
}