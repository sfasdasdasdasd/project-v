﻿




package networking.packets.server {

    import flash.utils.IDataInput;

    public class _iD_ extends _01Q_ {

    public function _iD_(_arg1:uint) {
        super(_arg1);
    }
    public var _type:int;
    public var text:String;

    override public function parseFromInput(_arg1:IDataInput):void {
        this._type = _arg1.readInt();
        this.text = _arg1.readUTF();
    }

    override public function toString():String {
        return (formatToString("GLOBAL_NOTIFICATION", "type", "text"));
    }

}
}//package networking.packets.server

