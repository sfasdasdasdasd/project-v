﻿




package networking.packets.server {

    import flash.utils.IDataOutput;

    import networking._098;

    public class _01Q_ extends _098 {

    public function _01Q_(_arg1:uint) {
        super(_arg1);
    }

    final override public function writeToOutput(_arg1:IDataOutput):void {
        throw (new Error((("Client should not send " + id_) + " messages")));
    }

}
}//package networking.packets.server

