﻿




package networking.packets.server {

    import flash.utils.IDataInput;

    public class InvResult extends _01Q_ {

    public function InvResult(_arg1:uint) {
        super(_arg1);
    }
    public var result_:int;

    override public function parseFromInput(_arg1:IDataInput):void {
        this.result_ = _arg1.readInt();
    }

    override public function toString():String {
        return (formatToString("INVRESULT", "result_"));
    }

}
}//package networking.packets.server

