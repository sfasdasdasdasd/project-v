﻿




package networking.packets.client {

    import flash.utils.IDataOutput;

    import networking._0_3;
    import networking._Q_0;

    public class _fI_ extends _R_q {

    public function _fI_(_arg1:uint) {
        this.slotObject_ = new _0_3();
        this.itemUsePos_ = new _Q_0();
        super(_arg1);
    }
    public var time_:int;
    public var slotObject_:_0_3;
    public var itemUsePos_:_Q_0;

    override public function writeToOutput(_arg1:IDataOutput):void {
        _arg1.writeInt(this.time_);
        this.slotObject_.writeToOutput(_arg1);
        this.itemUsePos_.writeToOutput(_arg1);
    }

    override public function toString():String {
        return (formatToString("USEITEM", "slotObject_", "itemUsePos_"));
    }

}
}//package networking.packets.client

