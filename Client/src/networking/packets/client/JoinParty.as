﻿




package networking.packets.client {

    import flash.utils.IDataOutput;

    public class JoinParty extends _R_q {

    public function JoinParty(_arg1:uint) {
        super(_arg1);
    }
    public var partyID_:int;

    override public function writeToOutput(_arg1:IDataOutput):void {
        _arg1.writeInt(this.partyID_);
    }

    override public function toString():String {
        return (formatToString("JOINPARTY", "partyID_"));
    }

}
}//package networking.packets.client

