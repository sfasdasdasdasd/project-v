﻿




package networking.packets.client {

    import flash.utils.IDataOutput;

    import networking._Q_0;

    public class _K_w extends _R_q {

    public function _K_w(_arg1:uint) {
        this.position_ = new _Q_0();
        super(_arg1);
    }
    public var time_:int;
    public var position_:_Q_0;

    override public function writeToOutput(_arg1:IDataOutput):void {
        _arg1.writeInt(this.time_);
        this.position_.writeToOutput(_arg1);
    }

    override public function toString():String {
        return (formatToString("GROUNDDAMAGE", "time_", "position_"));
    }

}
}//package networking.packets.client

