﻿




package _9W_ {

    import _0_p.IContext;

    import flash.events.Event;

    public class ModularContextEvent extends Event {

    public static const _qb:String = "contextAdd";
    public static const _L_D_:String = "contextRemove";

    public function ModularContextEvent(_arg1:String, _arg2:IContext) {
        super(_arg1, true, true);
        this._rA_ = _arg2;
    }
    private var _rA_:IContext;

    public function get context():IContext {
        return (this._rA_);
    }

    override public function clone():Event {
        return (new ModularContextEvent(type, this.context));
    }

    override public function toString():String {
        return (formatToString("ModularContextEvent", "context"));
    }

}
}//package _9W_

