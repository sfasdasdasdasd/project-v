﻿package _0_p {

    import _eZ_.IInjector;

    import _qj._pn;

    public interface IContext {

    function get injector():IInjector;

    function get _iL_():_0E_9;

    function get _39():uint;

    function set _39(_arg1:uint):void;

    function extend(..._args):IContext;

    function configure(..._args):IContext;

    function _ty(_arg1:_pn, _arg2:Function):IContext;

    function _0H_4(_arg1:Object):_Q_5;

    function _J_3(_arg1:_fq):IContext;

    function _W_b(..._args):IContext;

    function release(..._args):IContext;

}
}