﻿




package _M_T_ {

    import _03T_._078;

    import _0_p.IContext;
    import _0_p._D_v;

    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;

    public class _O_N_ implements _D_v {

    private const _ul:String = _078.create(_O_N_);

    public function _O_N_(_arg1:IEventDispatcher = null) {
        this._eventDispatcher = ((_arg1) || (new EventDispatcher()));
    }
    private var _eventDispatcher:IEventDispatcher;

    public function extend(_arg1:IContext):void {
        _arg1.injector.map(IEventDispatcher)._q3(this._eventDispatcher);
    }

    public function toString():String {
        return (this._ul);
    }

}
}//package _M_T_

