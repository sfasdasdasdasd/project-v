﻿package _G_A_ {

    import _03T_.Context;

    import _eZ_.IInjector;

    public class GameContext extends Context {

    public static var injector:IInjector;

    public static function getInjector():IInjector {
        return (injector);
    }

    public function GameContext() {
        if (!GameContext.injector) {
            GameContext.injector = this.injector;
        }
    }

}
}