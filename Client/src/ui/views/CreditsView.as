﻿




package ui.views {

    import _02t._R_f;

    import _sp.Signal;

    import com.company.utils.SimpleText;

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.filters.DropShadowFilter;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;

//    import com.company.rotmg.graphics.ScreenGraphic;
//    import com.company.rotmg.graphics.StackedLogoR;
    public class CreditsView extends Sprite {

    private static const _088:String = "http://www.wildshadow.com/";

    public function CreditsView() {
        this.close = new Signal();
        addChild(new _R_f());
//        addChild(new ScreenGraphic());
        this._0H_L_ = new SimpleText(16, 0xB3B3B3, false, 0, 0, "Myriad Pro");
        this._0H_L_.setBold(true);
        this._0H_L_.text = "Developed by:";
        this._0H_L_.updateMetrics();
        this._0H_L_.filters = [new DropShadowFilter(0, 0, 0)];
        addChild(this._0H_L_);
//        this._01q = new StackedLogoR();
//        this._01q.scaleX = (this._01q.scaleY = 1.2);
//        this._01q.addEventListener(MouseEvent.CLICK, this._W_w);
//        this._01q.buttonMode = true;
//        this._01q.useHandCursor = true;
//        addChild(this._01q);
        this._045 = new _H_o("close", 36, false);
        this._045.addEventListener(MouseEvent.CLICK, this._ly);
        addChild(this._045);
    }
    public var close:Signal;
    private var _0H_L_:SimpleText;
//    private var _01q:StackedLogoR;
    private var _045:_H_o;

    public function initialize():void {
        stage;
        this._0H_L_.x = ((800 / 2) - (this._0H_L_.width / 2));
        this._0H_L_.y = 10;
        stage;
//        this._01q.x = ((800 / 2) - (this._01q.width / 2));
//        this._01q.y = 50;
//        stage;
        this._045.x = ((800 / 2) - (this._045.width / 2));
        this._045.y = 524;
    }

    protected function _W_w(_arg1:Event):void {
        navigateToURL(new URLRequest(_088), "_blank");
    }

    private function _ly(_arg1:Event):void {
        this.close.dispatch();
    }

}
}//package ui.views

