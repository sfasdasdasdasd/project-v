﻿




package ui.elements {

    import flash.display.Shape;
    import flash.display.Sprite;
    import flash.events.Event;

    import ui.frames.Frame;

    public class FrameHolderNoDim extends Sprite {

    public function FrameHolderNoDim(frame:Frame) {
        this.frame = frame;
        this.frame.addEventListener(Event.COMPLETE, this.onComplete);
        addChild(this.frame);
    }
    private var dimScreen:Shape;
    private var frame:Frame;

    private function onComplete(_arg1:Event):void {
        parent.removeChild(this);
    }

}
}//package com.company.project_v.ui

