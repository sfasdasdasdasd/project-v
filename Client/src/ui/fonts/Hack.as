/**
 * Created by vooolox on 24-2-2016.
 */
package ui.fonts {

  import mx.core.FontAsset;

  [Embed(source="Hack.ttf", fontName="Hack_Bold", fontFamily="Mono", fontStyle="Regular", embedAsCFF="false", mimeType="application/x-font-truetype")]
  public class Hack extends FontAsset {

    public function Hack() {
      super();

    }
  }
}
